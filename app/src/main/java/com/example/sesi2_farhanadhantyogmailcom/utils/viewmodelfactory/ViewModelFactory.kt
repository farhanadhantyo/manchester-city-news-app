package com.example.sesi2_farhanadhantyogmailcom.utils.viewmodelfactory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.sesi2_farhanadhantyogmailcom.data.repository.Repository
import com.example.sesi2_farhanadhantyogmailcom.presentation.detail.viewmodel.DetailViewModel
import com.example.sesi2_farhanadhantyogmailcom.presentation.home.viewmodel.HomeViewModel
import javax.inject.Inject
import javax.inject.Provider

class ViewModelFactory @Inject constructor(private val viewModels: MutableMap<Class<out ViewModel>, Provider<ViewModel>>) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T = viewModels[modelClass]?.get() as T
}