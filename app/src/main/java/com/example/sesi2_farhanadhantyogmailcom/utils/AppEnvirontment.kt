package com.example.sesi2_farhanadhantyogmailcom.utils

object AppEnvirontment {

    const val DB_APP = "batch10.db"

    object ConstKey {
        // INTENT KEY
        const val EXTRA_ARTICLE = "EXTRA_ARTICLE"

        // PREFERENCES KEY
        const val PREF_EMAIL = "PREF_EMAIL"
    }

}