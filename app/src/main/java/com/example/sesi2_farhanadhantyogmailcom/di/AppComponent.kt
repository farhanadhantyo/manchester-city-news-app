package com.example.sesi2_farhanadhantyogmailcom.di

import com.example.sesi2_farhanadhantyogmailcom.presentation.detail.view.DetailActivity
import com.example.sesi2_farhanadhantyogmailcom.presentation.favorite.view.FavoriteActivity
import com.example.sesi2_farhanadhantyogmailcom.presentation.home.view.HomeFragment
import com.example.sesi2_farhanadhantyogmailcom.presentation.login.view.LoginActivity
import com.example.sesi2_farhanadhantyogmailcom.presentation.splash.view.SplashActivity
import com.example.sesi2_farhanadhantyogmailcom.utils.viewmodelfactory.ViewModelModule
import dagger.Component

@Component(modules = [AppModule::class, ViewModelModule::class])
interface AppComponent {

    fun inject(splashActivity: SplashActivity)

    fun inject(loginActivity: LoginActivity)

    fun inject(homeFragment: HomeFragment)

    fun inject(favoriteActivity: FavoriteActivity)

    fun inject(detailActivity: DetailActivity)

}