package com.example.sesi2_farhanadhantyogmailcom.presentation.favorite.view

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sesi2_farhanadhantyogmailcom.R
import com.example.sesi2_farhanadhantyogmailcom.data.model.article.Article
import com.example.sesi2_farhanadhantyogmailcom.data.repository.Repository
import com.example.sesi2_farhanadhantyogmailcom.databinding.ActivityFavoriteBinding
import com.example.sesi2_farhanadhantyogmailcom.di.AppModule
import com.example.sesi2_farhanadhantyogmailcom.di.DaggerAppComponent
import com.example.sesi2_farhanadhantyogmailcom.presentation.detail.view.DetailActivity
import com.example.sesi2_farhanadhantyogmailcom.presentation.favorite.viewmodel.FavoriteViewModel
import com.example.sesi2_farhanadhantyogmailcom.utils.AppEnvirontment
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class FavoriteActivity : AppCompatActivity() {

    private var _layout: ActivityFavoriteBinding? = null

    private val layout: ActivityFavoriteBinding
        get() = _layout ?: throw IllegalStateException("The activity has been destroyed")

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var mFavoriteViewModel: FavoriteViewModel
    private var mFavoriteAdapter: FavoriteAdapter? = null

    @SuppressLint("SimpleDateFormat")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityFavoriteBinding.inflate(layoutInflater)
        _layout = binding
        setContentView(binding.root)

        setSupportActionBar(layout.toolbar.root)
        supportActionBar?.apply {
            title = getString(R.string.title_favorite)
            setDisplayHomeAsUpEnabled(true)
        }

        DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build().inject(this)

        mFavoriteViewModel = ViewModelProvider(
            this,
            viewModelFactory
        )[FavoriteViewModel::class.java]

        mFavoriteAdapter = FavoriteAdapter { article ->
            val intent = Intent(this, DetailActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                putExtra(AppEnvirontment.ConstKey.EXTRA_ARTICLE, article)
            }
            startActivity(intent)
        }

        layout.rvArticle.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = mFavoriteAdapter
        }

        mFavoriteViewModel.getArticles()

        mFavoriteViewModel.articles.observe(this, Observer { articles ->
            articles?.let {
                mFavoriteAdapter?.setData(it)
            }

        })



    }

    override fun onDestroy() {
        super.onDestroy()
        _layout = null
    }
}